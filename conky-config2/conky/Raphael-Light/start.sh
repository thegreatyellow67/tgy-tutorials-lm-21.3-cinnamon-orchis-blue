#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.config/conky/Raphael-Light/config/clock-date.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Light/config/music-play.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Light/config/network.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Light/config/sysinfo.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Light/config/weather-info.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Light/config/workspaces.conf &> /dev/null &

exit
