#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.config/conky/Raphael-Dark/config/clock-date.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Dark/config/music-play.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Dark/config/network.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Dark/config/sysinfo.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Dark/config/weather-info.conf &> /dev/null &
conky -c $HOME/.config/conky/Raphael-Dark/config/workspaces.conf &> /dev/null &

exit
