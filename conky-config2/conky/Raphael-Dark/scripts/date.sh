#!/bin/bash

dayname=`date +"%A"`
daynumber=`date +"%d"`
month=`date +"%B"`
year=`date +"%Y"`

dayname=${dayname^}
month=${month^}

echo ${dayname}", "${daynumber}" "${month}" "${year}
