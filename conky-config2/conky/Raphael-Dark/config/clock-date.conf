conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  RAPHAEL DARK
--
--  Original Author : TheGreatYellow67
--  Created         : 2024/04/05
--  Variant         : Celsius & Italian language
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 monitor
 
--==============================================================================

-- Size and Position settings --
  alignment = 'top_left',
  gap_x = 50,
  gap_y = 50,
  maximum_width = 500,
  minimum_height = 200,
  minimum_width = 500,
  
-- Text settings --
  use_xft = true,
  override_utf8_locale = true,
  font = 'JetBrainsMono Nerd Font:size=9',
  
-- Color Settings --
  default_color = '#3B3B54',
  default_outline_color = '#3B3B54',
  default_shade_color = '#3B3B54',
  color1 = '#1972E7',
  
-- Window Settings --
  background = false,
  border_width = 1,
  draw_blended = false,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_colour = '000000',
  own_window_class = 'Conky',
  own_window_argb_visual = true,
  own_window_type = 'desktop',
  own_window_transparent = true,
  own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
  stippled_borders = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 1,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false,
}

conky.text = [[
${voffset 0}${font JetBrainsMono Nerd Font:size=45}${color1}${exec date +"%H"}${color}${exec date +":%M"}${font}
${voffset 5}${color}${font Oswald:size=20}${execi 200 ~/.config/conky/Raphael-Dark/scripts/date.sh}${font}
]]
