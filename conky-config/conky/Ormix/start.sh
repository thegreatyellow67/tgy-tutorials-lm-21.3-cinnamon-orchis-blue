#!/bin/bash

killall conky
sleep 2s

conky -c $HOME/.config/conky/Ormix/config/clock-date.conf &> /dev/null &
conky -c $HOME/.config/conky/Ormix/config/music-play.conf &> /dev/null &
conky -c $HOME/.config/conky/Ormix/config/weather-info.conf &> /dev/null &
conky -c $HOME/.config/conky/Ormix/config/workspaces.conf &> /dev/null &

exit
