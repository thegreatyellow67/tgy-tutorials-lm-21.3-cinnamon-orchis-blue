conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  ER-RAI
--  ( A part of Chepeus Conky themes pack )
--
--  Original Author : Closebox73
--  Created         : 2021/05/27
--  Variant         : Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/04/01
--  Variant         : Italian language
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 monitor

--==============================================================================

-- Size and Position settings --
  alignment = 'top_right',
  gap_x = 40,
  gap_y = 40,
  maximum_width = 300,
  minimum_height = 150,
  minimum_width = 150,
  
-- Text settings --
  use_xft = true,
  override_utf8_locale = true,
  font = 'Roboto Condensed:style=Regular:size=9',
  
-- Color Settings --
  default_color = '#FFFFFF',
  default_outline_color = '#fbf1c7',
  default_shade_color = '#fbf1c7',
  color1 = '#FFFFFF',
  
-- Window Settings --
  background = false,
  border_width = 1,
  draw_blended = false,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_colour = '000000',
  own_window_class = 'Conky',
  own_window_argb_visual = true,
  own_window_type = 'desktop',
  own_window_transparent = true,
  own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
  stippled_borders = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 2,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false
}

conky.text = [[
${execi 200 ~/.config/conky/Ormix/scripts/weather-v2.0.sh}\
${offset 0}${voffset 0}${font weather icons:size=48}${execi 15 ~/.config/conky/Ormix/scripts/weather-text-icon.sh}${font}
${offset 105}${voffset -52}${font Roboto Condensed:style=Regular:size=38}${execi 100 cat ~/.cache/weather.json | jq '.main.temp' | awk '{print int($1+0.5)}'}${voffset -12}${font Roboto Condensed:style=Regular:size=25}°C
${alignr}${voffset 5}${font Roboto Condensed:style=Regular:bold:size=12}${execi 100 cat ~/.cache/weather.json | jq -r '.name'} (${execi 100 cat ~/.cache/weather.json | jq -r '.sys.country'})
${alignr}${voffset 5}${font Roboto Condensed:style=Regular:bold:size=16}${execi 100 ~/.config/conky/Ormix/scripts/weather-description.sh}
${alignr}${voffset 0}${font Roboto Condensed:style=Regular:size=12}Temp. percepita ${execi 100 cat ~/.cache/weather.json | jq '.main.feels_like' | awk '{print int($1+0.5)}'}°C
]]
