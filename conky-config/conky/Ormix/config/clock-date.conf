conky.config = {
--==============================================================================

--  This theme is for conky version 1.10.8 or newer
--
--  ALSHAIN
--  ( A part of Eridanus Conky themes pack )
--
--  Original Author : Closebox73
--  Created         : 2023/03/25
--  Variant         : Celsius
--  Modified by     : TheGreatYellow67
--  Modified date   : 2024/04/01
--  Variant         : Italian language
--
--  License         : Distributed under the terms of GPLv3
--  Notes           : Created on 1920x1080 monitor
 
--==============================================================================

-- Size and Position settings --
  alignment = 'middle_middle',
  gap_x = 0,
  gap_y = -180,
  maximum_width = 484,
  minimum_height = 200,
  minimum_width = 484,
  
-- Text settings --
  use_xft = true,
  override_utf8_locale = true,
  font = 'Roboto:size=2',
  
-- Color Settings --
  default_color = '#FFFFFF',
  default_outline_color = '#FFFFFF',
  default_shade_color = '#FFFFFF',
  color1 = '#000000',
  
-- Window Settings --
  background = false,
  border_width = 1,
  draw_blended = false,
  draw_borders = false,
  draw_graph_borders = false,
  draw_outline = false,
  draw_shades = false,
  own_window = true,
  own_window_colour = '000000',
  own_window_class = 'Conky',
  own_window_argb_visual = true,
  own_window_type = 'desktop',
  own_window_transparent = true,
  own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
  stippled_borders = 0,
  
-- Others --
  cpu_avg_samples = 2,
  net_avg_samples = 1,
  double_buffer = true,
  out_to_console = false,
  out_to_stderr = false,
  extra_newline = false,
  update_interval = 1,
  uppercase = false,
  use_spacer = 'none',
  show_graph_scale = false,
  show_graph_range = false,
}

conky.text = [[
${image ~/.config/conky/Ormix/res/bg.png -p 0,0}\
${alignc 190}${voffset 10}${color1}${font Bebas Neue:size=80}${exec date +"%H" | cut -b 1}${font}
${alignc 75}${voffset -97}${color1}${font Bebas Neue:size=80}${exec date +"%H" | cut -b 2}${font}
${alignc -75}${voffset -97}${color1}${font Bebas Neue:size=80}${exec date +"%M" | cut -b 1}${font}
${alignc -190}${voffset -97}${color1}${font Bebas Neue:size=80}${exec date +"%M" | cut -b 2}${font}
${alignc}${voffset 50}${color}${font Bebas Neue:size=30}Oggi è ${time %A, %d %B %Y}${font}
]]
