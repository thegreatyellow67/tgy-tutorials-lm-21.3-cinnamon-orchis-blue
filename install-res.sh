#!/bin/bash

clear
wget https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-cinnamon-orchis-blue/-/raw/main/color-schemes &> /dev/null
source $(dirname $0)/color-schemes

THEME="orchis-blue"
CURSOR_THEME="colloid"
ICON_THEME="tela-circle"
THEME_FOLDER="lm-21.3-cinnamon-${THEME}"
BASE_PATH="${HOME}/Scaricati/"

echo -e "${blue}"
echo ""
echo "   ██████╗██╗███╗   ██╗███╗   ██╗ █████╗ ███╗   ███╗ ██████╗ ███╗   ██╗"
echo "  ██╔════╝██║████╗  ██║████╗  ██║██╔══██╗████╗ ████║██╔═══██╗████╗  ██║"
echo "  ██║     ██║██╔██╗ ██║██╔██╗ ██║███████║██╔████╔██║██║   ██║██╔██╗ ██║"
echo "  ██║     ██║██║╚██╗██║██║╚██╗██║██╔══██║██║╚██╔╝██║██║   ██║██║╚██╗██║"
echo "  ╚██████╗██║██║ ╚████║██║ ╚████║██║  ██║██║ ╚═╝ ██║╚██████╔╝██║ ╚████║"
echo "   ╚═════╝╚═╝╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═══╝"
echo ""                                                                     
echo "   ██████╗ ██████╗  ██████╗██╗  ██╗██╗███████╗    ██████╗ ██╗     ██╗   ██╗███████╗"
echo "  ██╔═══██╗██╔══██╗██╔════╝██║  ██║██║██╔════╝    ██╔══██╗██║     ██║   ██║██╔════╝"
echo "  ██║   ██║██████╔╝██║     ███████║██║███████╗    ██████╔╝██║     ██║   ██║█████╗"
echo "  ██║   ██║██╔══██╗██║     ██╔══██║██║╚════██║    ██╔══██╗██║     ██║   ██║██╔══╝"
echo "  ╚██████╔╝██║  ██║╚██████╗██║  ██║██║███████║    ██████╔╝███████╗╚██████╔╝███████╗"
echo "   ╚═════╝ ╚═╝  ╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝╚══════╝    ╚═════╝ ╚══════╝ ╚═════╝ ╚══════╝"
echo -e "${red}${bold}"
echo "  ========================================="
echo "  Script per automatizzare la copia delle"
echo "  risorse per il tema Cinnamon ${THEME^^}"
echo "  Scritto da TGY-TUTORIALS il 01/04/2024"
echo "  ========================================="
echo -e "${reset}"

function goto
{
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

echo "  Premi 's' per continuare o 'n' per uscire dallo script..."

# In attesa che l'utente prema un tasto
read -s -n 1 key

# Controlla se è stato premuto un tasto
case $key in
    s|S)
        goto main
        ;;
    n|N)
        echo "  Termino lo script...a presto!"
        exit 1
        ;;
    *)
        echo "  Tasto non valido. Per favore premi 's' o 'n'."
        sleep 5
        exit 1
        ;;
esac

main:

#
# Crea la cartella Scaricati se
# eventualmente mancante
#
if [ ! -d ${BASE_PATH} ];then
  mkdir -p ${BASE_PATH}
fi

cd ${BASE_PATH}

if [ ! -d ${THEME_FOLDER} ];then

  echo ""
  echo "  Sto per scaricare le risorse da GitLab, un pò di pazienza..."
  echo ""

  #
  # Installa git se mancante
  #
  if ! location="$(type -p "git")" || [ -z "git" ]; then
    echo "  Installo git per far funzionare questo script..."
    sudo apt install -y git &> /dev/null
  fi

  git clone https://gitlab.com/thegreatyellow67/tgy-tutorials-lm-21.3-cinnamon-${THEME}.git ${THEME_FOLDER}

  cd ${THEME_FOLDER}
  rm -fr .git

  clear
  echo ""
  echo "  Sto installando alcuni pacchetti utili, un pò di pazienza..."
  echo "  --------------------------------------------------------------"
  echo "  Pacchetti:"
  echo "  fonts-powerline | conky-all | jq | playerctl | vlc | ffmpeg"
  echo "  dconf-editor | tilix | gpick | gimp | inkscape | htop | btop"
  echo "  cava | gpaste | gir1.2-gpaste-1.0 | rofi | plymouth"
  echo "  --------------------------------------------------------------"
  echo ""
  sudo sudo apt install fonts-powerline conky-all jq playerctl vlc ffmpeg dconf-editor tilix gpick gimp inkscape htop btop cava gpaste gir1.2-gpaste-1.0 rofi plymouth -y &> /dev/null

  #
  # Crea cartelle eventualmente mancanti
  #
  if [ ! -d ~/.fonts ];then
    mkdir -p ~/.fonts
  fi

  if [ ! -d ~/.themes ];then
    mkdir -p ~/.themes
  fi

  if [ ! -d ~/.icons ];then
    mkdir -p ~/.icons
  fi

  if [ ! -d ~/.config/conky ];then
    mkdir -p ~/.config/conky
  fi

  if [ ! -d ~/.config/autostart ];then
    mkdir -p ~/.config/autostart
  fi

  if [ ! -d ~/.local/share/applications ];then
    mkdir -p ~/.local/share/applications
  fi

  if [ ! -d /boot/grub/themes ];then
    sudo mkdir -p /boot/grub/themes
  fi

  clear
  echo ""
  echo "  Sostituzione con utente corrente in"
  echo "  alcuni files di configurazione..."
  echo ""
  find cinnamon-applets/.config/cinnamon/spices/ -type f -name "*.json" -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find cinnamon-dconf/ -type f -name "*.conf" -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  find orchis-blue-backgrounds/.config/cinnamon/backgrounds/ -type f -name "*.lst" -exec sed -i "s/tgy-tutorials/${USER}/g" {} +
  sleep 3

  clear
  echo ""
  echo "  Installazione dei caratteri..."
  echo "  Caratteri utilizzati in questo tema:"
  echo ""
  echo "  IBM Plex Sans:"
  echo "  --------------------------------"
  echo "  https://fonts.google.com/specimen/IBM+Plex+Sans"
  echo ""
  echo "  FiraCode Nerd Font:"
  echo "  --------------------------------"
  echo "  https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/FiraCode.zip"
  echo ""
  cp -r ${THEME}-fonts/* ~/.fonts
  sudo cp -r ${THEME}-fonts/IBMPlexSans/ /usr/share/fonts/truetype/
  sudo cp -r ${THEME}-fonts/FiraCode/ /usr/share/fonts/truetype/
  fc-cache -fr
  sudo fc-cache -fr
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema GTK ${THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonti:"
  echo "  --------------------------------"
  echo "  https://github.com/vinceliuice/Orchis-theme"
  echo "  https://www.gnome-look.org/p/1357889/"
  echo ""
  cp -r ${THEME}-gtk-themes/* ~/.themes
  sudo cp -r ${THEME}-gtk-themes/* /usr/share/themes
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone ${ICON_THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonti:"
  echo "  --------------------------------"
  echo "  https://github.com/vinceliuice/Tela-circle-icon-theme"
  echo "  https://www.gnome-look.org/p/1359276"
  echo ""
  cp -r ${THEME}-icons/* ~/.icons
  sudo cp -r ${THEME}-icons/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione dei cursori ${CURSOR_THEME^^}, un pò di pazienza..."
  echo ""
  echo "  Fonte:"
  echo "  --------------------------------"
  echo "  https://www.gnome-look.org/p/1831077"
  echo ""
  cp -r ${THEME}-cursors/* ~/.icons
  sudo cp -r ${THEME}-cursors/* /usr/share/icons
  sleep 3

  clear
  echo ""
  echo "  Installazione delle icone per le applets..."
  echo ""
  cp -r applets-icons/ ~/.icons
  sleep 3

  clear
  echo ""
  echo "  Installazione delle applets e relative configurazioni..."
  echo ""
  cp -r cinnamon-applets/.config/cinnamon/spices/* ~/.config/cinnamon/spices/
  cp -r cinnamon-applets/.local/share/cinnamon/applets/* ~/.local/share/cinnamon/applets/
  sleep 3

  clear
  echo ""
  echo "  Installazione delle estensioni e relative configurazioni..."
  echo ""
  cp -r cinnamon-extensions/.config/cinnamon/spices/* ~/.config/cinnamon/spices/
  cp -r cinnamon-extensions/.local/share/cinnamon/extensions/* ~/.local/share/cinnamon/extensions/
  sleep 3

  clear
  echo ""
  echo "  Installazione degli sfondi..."
  echo ""
  cp -r ${THEME}-backgrounds/* ~/Immagini
  cp -r orchis-blue-backgrounds/.config/cinnamon/backgrounds/ ~/.config/cinnamon/
  sleep 3

  clear
  echo ""
  echo "  Installazione delle azioni per Nemo..."
  echo ""
  cp -r nemo-actions/* ~/.local/share/nemo/actions
  sleep 3

  clear
  echo ""
  echo "  Installazione dei temi per Oh My Posh (per la bash)..."
  echo ""
  cp -r oh-my-posh-themes/.oh-my-posh-themes/ ~/
  sleep 3

  clear
  echo ""
  echo "  Installazione dello script di Conky Ormix..."
  echo ""
  cp -r conky-config/conky/* ~/.config/conky
  cp conky-config/autostart/* ~/.config/autostart
  cp conky-config/applications/* ~/.local/share/applications
  sudo cp conky-config/script/launch-conky /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-conky
  sleep 3

  clear
  echo ""
  echo "  Installazione dello script di Conky Raphael..."
  echo ""
  cp -r conky-config2/conky/* ~/.config/conky
  cp conky-config2/applications/* ~/.local/share/applications
  sudo cp conky-config2/script/launch-conkyr /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-conkyr
  sleep 3

  WIFI_DEVICE=`nmcli device | grep wifi\ | awk '{print $1}'`
  LAN_DEVICE=`ip -br l | awk '$1 !~ "lo|vir|wl" { print $1}'`

  # Sostituzione dei nomi dipositivo per
  # il wireless e/o la ethernet  
  if [[ "${WIFI_DEVICE}" != "" ]]; then
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Raphael-Dark/config/network.conf
    sed -i "s/wlan0/${WIFI_DEVICE}/g" ~/.config/conky/Raphael-Light/config/network.conf
  fi

  if [[ "${LAN_DEVICE}" != "" ]]; then
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Raphael-Dark/config/network.conf
    sed -i "s/enp0s3/${LAN_DEVICE}/g" ~/.config/conky/Raphael-Light/config/network.conf
  fi
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Cava..."
  echo ""
  cp -r cava-config/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Glava..."
  echo ""
  cp -r glava-config/.config/glava/ ~/.config
  cp glava-config/.config/autostart/* ~/.config/autostart
  cp glava-config/.local/share/applications/* ~/.local/share/applications
  sudo cp glava-config/script/launch-glava /usr/local/bin
  sudo chmod 755 /usr/local/bin/launch-glava
  sleep 3

  clear
  echo ""
  echo "  Installazione dello sfondo per la finestra di accesso..."
  echo ""
  sudo cp -r login-window/ /usr/share/backgrounds
  sleep 3

  clear
  echo ""
  echo "  Installazione di macchina e relativa configurazione..."
  echo ""
  sudo cp macchina/macchina-linux-x86_64 /usr/local/bin/macchina
  sudo chmod +x /usr/local/bin/macchina
  cp -r macchina/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di neofetch..."
  echo ""
  cp -r neofetch/ ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Rofi..."
  echo ""
  cp -r rofi-config/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione della configurazione di Ulauncher..."
  echo ""
  cp -r ulauncher/ulauncher-theme/.config/* ~/.config
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema spinner_alt per Plymouth..."
  echo ""
  echo "  Nel terminale si dovranno poi eseguire i seguenti comandi:"
  echo "  --------------------------------"
  echo "  $ sudo update-alternatives --config default.plymouth (scegliere tema 3)"
  echo "  $ sudo update-initramfs -u"
  echo ""
  sudo cp -r plymouth-theme/spinner_alt/ /usr/share/plymouth/themes/
  sudo update-alternatives --install /usr/share/plymouth/themes/default.plymouth default.plymouth /usr/share/plymouth/themes/spinner_alt/spinner_alt.plymouth 100
  sleep 3

  clear
  echo ""
  echo "  Installazione del tema Sugar Candy Orchis Blue per Grub..."
  echo ""
  sudo cp -r sugar-candy-orchis-blue/ /boot/grub/themes
  sleep 3

  clear
  echo ""
  echo "  Modifica della disposizione dei pulsanti finestra Mac Classico..."
  echo ""
  sudo cp /usr/share/cinnamon/cinnamon-settings/modules/cs_windows.py /usr/share/cinnamon/cinnamon-settings/modules/cs_windows.py.bak
  old_layout='\["close:minimize,maximize", _("Classic Mac")\]'
  new_layout='\["close,minimize,maximize", _("Classic Mac")\]'
  file_path="/usr/share/cinnamon/cinnamon-settings/modules/cs_windows.py"
  sudo sed -i "s/${old_layout}/${new_layout}/g" ${file_path}
  sleep 3

  clear
  echo ""
  echo "  Importazione files di configurazione di dconf"
  echo "  (impostazioni di Cinnamon, configurazione di gnome-terminal)..."
  echo "  --------------------------------"
  echo "  Riavviare Cinnamon con:"
  echo "  Alt+F2"
  echo "  digitare r [ INVIO ]"
  echo ""
  dconf load / < cinnamon-dconf/cinnamon-orchis-blue.conf
  dconf load /org/gnome/terminal/ < cinnamon-dconf/gnome-terminal.conf
  sleep 3

  clear
  echo ""
  echo "  Risorse installate con successo!"
  sleep 3
  echo ""

else
  echo "  La cartella ${THEME_FOLDER} esiste! se vuoi scaricare"
  echo "  le risorse nuovamente devi cancellare la cartella e"
  echo "  rieseguire questo script in un terminale. Buona giornata!"
  echo ""
fi
